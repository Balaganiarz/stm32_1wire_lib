/*
 * rs232.h
 *
 *  Created on: 2017-08-05
 *       author: Jakub G�rski
 */

#include "stm32l1xx.h"
#include "core_cm3.h"
#include <stdlib.h>
#include "rs232.h"

void InitUART3(uint32_t baud_rate, uint32_t clock_source)
{


		// turn on PORTB clock
		RCC->AHBENR |= RCC_AHBLPENR_GPIOBLPEN;

		// turn on USART3 clock
		RCC->APB1ENR |= RCC_APB1ENR_USART3EN;

		/************************************/
		/*  USART3 config */
		/************************************/
		/* PB10 - TX */
		// Alternative function- AF7
		GPIOB->AFR[1] |= 0x00000700;

		// output 40 MHz
		GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR10;

		// push-pull
		GPIOB->OTYPER &= ~(GPIO_OTYPER_OT_10);

		// pull to vcc
		GPIOB->PUPDR |= GPIO_PUPDR_PUPDR10_0;

		// Alternative function
		GPIOB->MODER |= GPIO_MODER_MODER10_1;


		/************************************/
		/* USART3 */
		//  baudrate
		USART3->BRR = clock_source/baud_rate;


		// 8-bitowa, no parity
		USART3->CR1 &= ~(USART_CR1_M|USART_CR1_PCE);
		// 1 stop bit
		USART3->CR2 &= ~USART_CR2_STOP;
	    // enable USART3
		USART3->CR1 |= USART_CR1_TE|USART_CR1_UE;
}

// send char
void UART3_putc(char data)
{

	// wait for empty buffer
	while((USART3->SR & USART_SR_TXE) != USART_SR_TXE);
	// send data
	USART3->DR = (data & USART_DR_DR);
}


//send string
void UART3_puts(char *s)
{
	  while (*s)
	  {
		  UART3_putc(*s++);
	  }
}

// send int
void UART3_putint(int value, int radix)
{
	char string[17];
	itoa(value, string, radix);		// ASCII convertion
	UART3_puts(string);
}


