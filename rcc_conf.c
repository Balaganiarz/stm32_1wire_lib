#include "stm32l1xx.h"

#include "rcc_conf.h"

void RCC_conf_PLL()
{
	// reset RCC
	  /* Set MSION bit */
	  RCC->CR |= (uint32_t)0x00000100;

	  /* Reset SW[1:0], HPRE[3:0], PPRE1[2:0], PPRE2[2:0], MCOSEL[2:0] and MCOPRE[2:0] bits */
	  RCC->CFGR &= (uint32_t)0x88FFC00C;

	  /* Reset HSION, HSEON, CSSON and PLLON bits */
	  RCC->CR &= (uint32_t)0xEEFEFFFE;

	  /* Reset HSEBYP bit */
	  RCC->CR &= (uint32_t)0xFFFBFFFF;

	  /* Reset PLLSRC, PLLMUL[3:0] and PLLDIV[1:0] bits */
	  RCC->CFGR &= (uint32_t)0xFF02FFFF;

	  /* Disable all interrupts */
	  RCC->CIR = 0x00000000;

	// turn on HSE
	RCC->CR |= RCC_CR_HSEON;

	// wait till HSE ready
	while( (RCC->CR & RCC_CR_HSERDY) != RCC_CR_HSERDY);



	// if HSE ready
	if(RCC->CR & RCC_CR_HSERDY)
	{
	    /* Enable 64-bit access */
	    FLASH->ACR |= FLASH_ACR_ACC64;

	    /* Enable Prefetch Buffer */
	    FLASH->ACR |= FLASH_ACR_PRFTEN;

	    /* Flash 1 wait state */
	    FLASH->ACR |= FLASH_ACR_LATENCY;

		//
		// core prescaler - 1
	    // prescaler AHB - 1
		// prescaler APB1 - 1
		// prescaler  APB2 - 1
		RCC->CFGR &= ~(RCC_CFGR_MCOPRE|RCC_CFGR_PPRE1|RCC_CFGR_PPRE2|RCC_CFGR_HPRE ) ;

		// pll settings - div 2 i mult 8
		// PLL source - HSE
		RCC->CFGR |= RCC_CFGR_PLLSRC_HSE|RCC_CFGR_PLLDIV2|RCC_CFGR_PLLMUL8;

		// turn on PLL
		RCC->CR |= RCC_CR_PLLON;

		// wait till PLL ready
		while( (RCC->CR & RCC_CR_PLLRDY) != RCC_CR_PLLRDY);

	    /* Select PLL as system clock source */
	    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
	    RCC->CFGR |= (uint32_t)RCC_CFGR_SW_PLL;

	    /* Wait till PLL is used as system clock source */
	    while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != (uint32_t)RCC_CFGR_SWS_PLL)
	    {
	    }

	}

}

void RCC_conf_HSE()
{
	// reset RCC
	  /* Set MSION bit */
	  RCC->CR |= (uint32_t)0x00000100;

	  /* Reset SW[1:0], HPRE[3:0], PPRE1[2:0], PPRE2[2:0], MCOSEL[2:0] and MCOPRE[2:0] bits */
	  RCC->CFGR &= (uint32_t)0x88FFC00C;

	  /* Reset HSION, HSEON, CSSON and PLLON bits */
	  RCC->CR &= (uint32_t)0xEEFEFFFE;

	  /* Reset HSEBYP bit */
	  RCC->CR &= (uint32_t)0xFFFBFFFF;

	  /* Reset PLLSRC, PLLMUL[3:0] and PLLDIV[1:0] bits */
	  RCC->CFGR &= (uint32_t)0xFF02FFFF;

	  /* Disable all interrupts */
	  RCC->CIR = 0x00000000;

	// turn on HSE
	RCC->CR |= RCC_CR_HSEON;

	// wait till HSE ready
	while( (RCC->CR & RCC_CR_HSERDY) != RCC_CR_HSERDY);



	// if HSE ready
	if(RCC->CR & RCC_CR_HSERDY)
	{

		// core prescaler - 1
	    // prescaler AHB - 1
		// prescaler APB1 - 1
		// prescaler  APB2 - 1
		RCC->CFGR &= ~(RCC_CFGR_MCOPRE|RCC_CFGR_PPRE1|RCC_CFGR_PPRE2 ) ;

		// set system clock - HSE
		RCC->CFGR |= RCC_CFGR_SW_HSE;

		// wait till HSE is used as system clock source
		while((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSE);

	}

}

void RCC_conf_HSI()
{
	// reset  RCC
	  /* Set MSION bit */
	  RCC->CR |= (uint32_t)0x00000100;

	  /* Reset SW[1:0], HPRE[3:0], PPRE1[2:0], PPRE2[2:0], MCOSEL[2:0] and MCOPRE[2:0] bits */
	  RCC->CFGR &= (uint32_t)0x88FFC00C;

	  /* Reset HSION, HSEON, CSSON and PLLON bits */
	  RCC->CR &= (uint32_t)0xEEFEFFFE;

	  /* Reset HSEBYP bit */
	  RCC->CR &= (uint32_t)0xFFFBFFFF;

	  /* Reset PLLSRC, PLLMUL[3:0] and PLLDIV[1:0] bits */
	  RCC->CFGR &= (uint32_t)0xFF02FFFF;

	  /* Disable all interrupts */
	  RCC->CIR = 0x00000000;

	// turn on HSI
	RCC->CR |= RCC_CR_HSION;

	// wait till HSI is on
	while( (RCC->CR & RCC_CR_HSIRDY) != RCC_CR_HSIRDY);

	// core prescaler - 1
    // prescaler AHB - 1
	// prescaler APB1 - 1
	// prescaler  APB2 - 1
	RCC->CFGR &= ~(RCC_CFGR_MCOPRE|RCC_CFGR_PPRE1|RCC_CFGR_PPRE2 ) ;

	// set system clock - HSI
	RCC->CFGR |= RCC_CFGR_SW_HSI;
	// wait till HSI is used as system clock source
	while( (RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI);

}
