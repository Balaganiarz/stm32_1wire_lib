# README #

### What is this repository for? ###

This repository contains 1wire library for STM32l1xxx. Sample program
shows temperature measurement using up to 4 operating sensors the parasitic 
power mode. The source code was written in the CooCox environment.

Version 1.0

### How do I get set up? ###

To run test program one need to set up connections as shown in the diagram.
The 1Wire date is conected to port PC0, the TX and RX of USART are conected
to ports PB10 and PB11.
For communication with PC, there is recomended USART/USB coverter.

![Scheme](diagram_bit.png)

### Contribution guidelines ###

To change the 1Wire pin, you must change the following macro definitions
in **onewire.h** file:
```
#define OW_PIN_IN 		GPIO_IDR_IDR_0
#define OW_IN   		GPIOC->IDR

#define OW_PIN_OUT 		(uint16_t) GPIO_BSRR_BS_0
#define OW_OUT_H  		GPIOC->BSRRL
#define OW_OUT_L  		GPIOC->BSRRH

#define OW_MODE  		GPIOC->MODER
#define OW_MODE_IN 		GPIO_MODER_MODER0
#define OW_MODE_OUT		GPIO_MODER_MODER0_0

#define OW_RCC()  		RCC->AHBENR |= RCC_AHBLPENR_GPIOCLPEN
#define OW_SPEED() 		GPIOC->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR0_1
#define OW_OPEN_DRAIN()	GPIOC->OTYPER 	|= OW_PIN_IN
#define OW_PUSH_PULL() 	GPIOC->OTYPER 	&= ~OW_PIN_IN
```
To bus can be connectet up to 4 sensors. To modify this numer one must 
change `MAXSENSORS` macro in **ds18x28.h**.

Another worth mention issue about code is clock frequency. For time
accuracy of used delays, at least 32 MHz is required. If you intend 
to change clock source, be sure to change it definition in **delay.c**.

### Who do I talk to? ###

If have problems with sorce code, please contact with repo owner.
