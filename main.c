#include "stm32l1xx.h"

#include "rs232.h"
#include "rcc_conf.h"
#include "OneWire/onewire.h"
#include "OneWire/delay.h"
#include "OneWire/ds18x20.h"

uint8_t cnt_sensors;
uint8_t subzero, cel, cel_fract_bits;

void display_temp( );

int main(void)
{

	uint8_t var;
	RCC_conf_PLL();

	// uart3 init
	InitUART3(9600,32000000);

	// init one_wire_port
	init_onewire_port();

	// serch sensors connected to bus
	cnt_sensors = search_sensors();

	_delay_ms(750);

	// serch sensors connected to bus
	DS18X20_start_meas( DS18X20_POWER_PARASITE, NULL );
	// time required for 12-bit conversion (see note)
	_delay_ms(750);
	for (var = 0; var < cnt_sensors; ++var) {
    	if( DS18X20_OK == DS18X20_read_meas(gSensorIDs[var], &subzero, &cel, &cel_fract_bits) ){
    		// display result
    		display_temp();
    	}
    	else {
    		UART3_puts("error \r\n");
    	}
	}
	UART3_puts("\r\n");

    while(1)
    {
    	uint8_t *cl=(uint8_t*)gSensorIDs;	// get sensor adreses
    	for( uint8_t i=0; i<MAXSENSORS*OW_ROMCODE_SIZE; i++) *cl++ = 0; // clear all adreses fro memory

    	cnt_sensors = search_sensors();

    	_delay_ms(750);

    	DS18X20_start_meas( DS18X20_POWER_PARASITE, NULL );

    	_delay_ms(750);
    	for (var = 0; var < cnt_sensors; ++var) {
        	if( DS18X20_OK == DS18X20_read_meas(gSensorIDs[var], &subzero, &cel, &cel_fract_bits) ){
        		display_temp();
        	}
        	else {
        		UART3_puts("error \r\n");
        	}
		}
    	UART3_puts("\r\n");

    }
}

void display_temp( )
{
	if(subzero) UART3_puts("-");
	UART3_putint(cel,10);
	UART3_puts(".");
	UART3_putint(cel_fract_bits,10);
	UART3_puts("C \r\n");
}
