/************************************************************************/
/*                                                                      */
/*        Access Dallas 1-Wire Device with ATMEL AVRs                   */
/*                                                                      */
/*              Author: Peter Dannegger                                 */
/*                      danni@specs.de                                  */
/*                                                                      */
/* modified by Martin Thomas <eversmith@heizung-thomas.de> 9/2004       */
/************************************************************************/
/*
 * onewire.h
 *
 *  Created on: 2009-08-22
 *      modified by: Miros�aw Karda�
 */
/*
 * onewire.h
 *
 *  Created on: 2017-08-05
 *       modified by: Jakub G�rski
 */
#ifndef ONEWIRE_H_
#define ONEWIRE_H_

#include <inttypes.h>

/*******************************************/
/* Hardware connection                     */
/*******************************************/

/* 1Wire PIN selection */
// selected PC0
#define OW_PIN_IN 		GPIO_IDR_IDR_0
#define OW_IN   		GPIOC->IDR

#define OW_PIN_OUT 		(uint16_t) GPIO_BSRR_BS_0
#define OW_OUT_H  		GPIOC->BSRRL
#define OW_OUT_L  		GPIOC->BSRRH

#define OW_MODE  		GPIOC->MODER
#define OW_MODE_IN 		GPIO_MODER_MODER0
#define OW_MODE_OUT		GPIO_MODER_MODER0_0

#define OW_RCC()  		RCC->AHBENR |= RCC_AHBLPENR_GPIOCLPEN
#define OW_SPEED() 		GPIOC->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR0_1 // 10 MHz
#define OW_OPEN_DRAIN()	GPIOC->OTYPER 	|= OW_PIN_IN
#define OW_PUSH_PULL() 	GPIOC->OTYPER 	&= ~OW_PIN_IN

#define OW_GET_IN()   ( OW_IN & OW_PIN_IN)
#define OW_OUT_LOW()  ( OW_OUT_L = OW_PIN_OUT )
#define OW_OUT_HIGH() ( OW_OUT_H = OW_PIN_OUT )
#define OW_DIR_IN()   ( OW_MODE &= ~OW_MODE_IN )
#define OW_DIR_OUT()  ( OW_MODE |= OW_MODE_OUT )


/*******************************************/

#define OW_MATCH_ROM	0x55
#define OW_SKIP_ROM	    0xCC
#define	OW_SEARCH_ROM	0xF0
#define	OW_SEARCH_FIRST	0xFF		// start new search
#define	OW_PRESENCE_ERR	0xFF
#define	OW_DATA_ERR	    0xFE
#define OW_LAST_DEVICE	0x00		// last device found
//			0x01 ... 0x40: continue searching

// rom-code size including CRC
#define OW_ROMCODE_SIZE 8

void ow_init_port(void);

uint8_t ow_reset(void);

uint8_t ow_bit_io( uint8_t b );
uint8_t ow_byte_wr( uint8_t b );
uint8_t ow_byte_rd( void );

uint8_t ow_rom_search( uint8_t diff, uint8_t *id );

void ow_command( uint8_t command, uint8_t *id );

void ow_parasite_enable(void);
void ow_parasite_disable(void);
uint8_t ow_input_pin_state(void);



#endif /* ONEWIRE_H_ */
