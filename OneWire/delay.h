/*
 * delay.h
 *
 *  Created on: 6-08-2017
 *      Author: Jakub G�rski
 */

#ifndef DELAY_H_
#define DELAY_H_

void _delay_init(void);
void _delay_us(uint16_t time_us);
void _delay_ms(uint16_t time_ms);

#endif /* DELAY_H_ */
