/*
 * delay.h
 *
 *  Created on: 6-08-2017
 *      Author: Jakub G�rski
 */

#include "stm32l1xx.h"
#include "core_cm3.h"

#include "delay.h"

void _delay_init(void){
	// enable clock to TIM2
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;

	// set autoload register to max uin16
	TIM2->ARR = 65535;

	// default up_counting
	// enable TIM2
	TIM2->CR1 |= TIM_CR1_CEN;
	TIM2->EGR |= TIM_EGR_UG;
}

void _delay_init_us(void)
{
	// prescaler for 1 us if clock source is 32 MHz
	TIM2->PSC =  31;
	TIM2->EGR |= TIM_EGR_UG;

}

void _delay_init_ms(void)
{

	// prescaler for 1 ms if clock source is 32 MHz
	TIM2->PSC =  31999;
	TIM2->EGR |= TIM_EGR_UG;

}

void _delay_stop(void)
{
	// reset timer 2 value
	TIM2->EGR |= TIM_EGR_UG;

	// disable TIM2
	TIM2->CR1 &= ~TIM_CR1_CEN;


	// disable clock to TIM2
	RCC->APB1ENR &= ~RCC_APB1ENR_TIM2EN;

}

void _delay_us(uint16_t time_us)
{

	volatile uint16_t s = TIM2->CNT;
	while((TIM2->CNT-s) <= time_us);
	// reset timer 2 value
	TIM2->EGR |= TIM_EGR_UG;


}

void _delay_ms(uint16_t time_ms)
{
	_delay_init_ms();

	volatile uint16_t s = TIM2->CNT;
	while((TIM2->CNT-s) <= time_ms);

	_delay_init_us();

}


