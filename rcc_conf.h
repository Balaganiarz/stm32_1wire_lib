#ifndef RCC_CONF_H_
#define RCC_CONF_H_

// config system clock as PLL
void RCC_conf_PLL(void);
// config system clock as HSE
void RCC_conf_HSE(void);
// config system clock as HSI
void RCC_conf_HSI(void);

#endif
