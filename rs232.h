/*
 * rs232.h
 *
 *  Created on: 2017-08-05
 *      author: Jakub G�rski
 */

#ifndef RS232_H_
#define RS232_H_

void InitUART3(uint32_t baud_rate, uint32_t clock_source);
void UART3_putc(char data );
void UART3_puts(char* s );
void UART3_putint(int value, int radix);

#endif
